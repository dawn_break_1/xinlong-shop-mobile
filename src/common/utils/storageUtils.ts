/**
 * 存储相关工具类
 */

/**
 * 保存内容到本地缓存
 * @param key 本地缓存的key值
 * @param value 需要存储的内容，只支持原生类型、及能够通过 JSON.stringify 序列化的对象
 * @param expire 过期时间，默认0 表示永久有效
 */
export const setStorage = (
  key: string,
  value: any,
  expire: number = 0
): void => {
  let obj = {
    data: value,
    time: Date.now() / 1000,
    expire: expire,
  };
  uni.setStorageSync(key, JSON.stringify(obj));
};

/**
 * 获取保存在本地缓存的数据
 * @param key 保存本地缓存数据的key值
 * @returns 获取到的数据
 */
export const getStorage = (key: string): any => {
  try {
    let val = uni.getStorageSync(key);
    if (!val) {
      return null;
    }
    val = JSON.parse(val);
    if (val.expire && Date.now() / 1000 - val.time > val.expire) {
      uni.removeStorageSync(key);
      return null;
    }
    return val.data;
  } catch (e) {
    return null;
  }
};

/**
 * 删除缓存
 * @param key 保存本地缓存数据的key值
 * @returns 获取到的数据
 */
export const removeStorage = (key: string): void => {
  uni.removeStorageSync(key);
};
