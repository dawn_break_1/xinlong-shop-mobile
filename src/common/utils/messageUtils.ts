/**
 * 信息提示弹出框（官方内置）
 */

/**
 * Toast弹窗配置类型
 */
interface ToastOptions {
  /**
   * 标题
   */
  title: string,
  /**
   * 显示icon的类型
   */
  icon?: 'success' | 'loading' | 'error' | 'none',
  /**
   * 使用图片代替icon
   */
  image?: string,
  /**
   * 是否显示遮罩
   */
  mask?: boolean,
  /**
   * 关闭时间，单位ms
   */
  duration?: number
}

/**
 * Modal弹框配置项
 */
interface ModalOptions {
  /**
   * 弹框标题
   */
  title: string,
  /**
   * 弹框内容
   */
  content: string,
  /**
   * 是否显示取消按钮
   */
  showCancel?: boolean,
  /**
   * 取消按钮文本
   */
  cancelText?: string,
  /**
   * 确认按钮文本
   */
  confirmText?: string
}

/**
 * Loading弹框配置项
 */
interface LoadingOptions {
  /**
   * 标题
   */
  title?: string,
  /**
   * 是否显示遮罩
   */
  mask?: boolean
}

/**
 * 弹出内置Toast
 * @param options 配置信息
 * @returns 成功返回true，失败返回false（Promise）
 */
export const toast = (options: ToastOptions): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    const duration = options.duration ?? 1500
    uni.showToast({
      title: options.title,
      icon: options?.icon ?? 'none',
      image: options?.image ?? '',
      mask: options?.mask ?? true,
      duration,
      success: () => {
        setTimeout(() => {
          resolve(true)
        }, duration);
      },
      fail: () => {
        reject()
      }
    })
  })
}

/**
 * 关闭系统内置Toast
 */
export const closeToast = () => {
  uni.hideToast()
}

/**
 * 弹出系统内置Modal
 * @param options 弹框配置信息
 * @returns 用户选择情况（点击确认返回true，点击取消返回false）
 */
export const modal = (options: ModalOptions): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    uni.showModal({
      title: options.title,
      content: options.content,
      showCancel: options?.showCancel ?? false,
      cancelText: options?.cancelText ?? '取消',
      cancelColor: '#080808',
      confirmText: options?.confirmText ?? '确认',
      confirmColor: '#01BEFF',
      success: (res) => {
        if (res.confirm) {
          resolve(true)
        } else {
          resolve(false)
        }
      },
      fail: () => {
        reject()
      }
    })
  })
}

/**
 * 弹出内置Loading弹框
 * @param options 弹框配置信息
 */
export const loading = (options: LoadingOptions): void => {
  uni.showLoading({
    title: options?.title ?? '',
    mask: options?.mask ?? true
  })
}

/**
 * 关闭内置Loading弹框
 */
export const closeLoading = () => {
  uni.hideLoading()
}