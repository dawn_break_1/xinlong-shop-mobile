/**
 * 获取节点信息工具
 * @author jaylen
 */

import { ComponentInternalInstance } from "vue"

/**
 * 查询节点信息
 * @param selector 节点标识
 * @param component 当前组件对象
 * @returns 查询到的节点信息
 */
export const getRectInfo = (selector: string, component: ComponentInternalInstance): Promise<UniApp.NodeInfo | null> => {
  return new Promise((resolve, reject) => {
    uni.createSelectorQuery()
    .in(component)
    .select(selector)
    .boundingClientRect((rect: UniApp.NodeInfo) => {
      if (rect) {
        resolve(rect)
      } else {
        resolve(null)
      }
    })
    .exec()
  })
}