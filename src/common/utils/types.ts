/**
 * 类型判断
 * @author jaylen
 */
import { array } from './testUtils'

/**
 * 判断是否为数组类型
 */
export const isArray = (value?: any): value is any[] => array(value)