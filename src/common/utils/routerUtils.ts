/**
 * 页面/路由跳转工具
 * @author jaylen
 */
import { indexUrl as defaultIndexUrl} from '../config'

/**
 * 返回上一级路由
 * 如果不存在则重定向到首页
 * @param indexUrl 当前程序首页路由地址
 */
export const navBack = (indexUrl: string = ''): void => {
  indexUrl = indexUrl || defaultIndexUrl
  // 通过判断当前页面的页面栈信息，是否有上一页进行返回，如果没有则跳转到首页
  const pages = getCurrentPages()
  if (pages?.length > 0) {
    const firstPage = pages[0]
    if (pages.length === 1 && (!firstPage.route || firstPage.route != indexUrl)) {
      uni.reLaunch({
        url: indexUrl
      })
    } else {
      uni.navigateBack({
        delta: 1
      })
    }
  } else {
    uni.reLaunch({
      url: indexUrl
    })
  }
}

/**
 * 重定向到首页
 * @param indexUrl 首页地址
 */
export const navHome = (indexUrl: string = ''): void => {
  indexUrl = indexUrl || defaultIndexUrl
  uni.reLaunch({
    url: indexUrl
  })
}

/**
 * 跳转到指定的页面
 */
export const navPage = (url: string, events?:any ): void => {
  uni.navigateTo({
    url: url,
    events
  })
}

/**
 * 跳转登录页  会传递跳转前页面
 */
export const navLogin = (): void => {
  let routes = getCurrentPages();
  let toUrl = routes[routes.length - 1].$page.fullPath; //获取当前页面
  uni.navigateTo({
    url: "/pages/mine/login?toUrl=" + toUrl
  })
}


/**
 * 重定向到指定的页面
 */
export const navRedirecte = (url: string, ): void => {
  uni.redirectTo({
    url: url
  })
}