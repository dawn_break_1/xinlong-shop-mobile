/**
 * 颜色处理工具
 * @author jaylen
 */

import { getRandom } from './utils'

// 图鸟内置颜色名称
export const colorList: string[] = [
  'red',
  'purplered',
  'purple',
  'bluepurple',
  'aquablue',
  'blue',
  'indigo',
  'cyan',
  'teal',
  'green',
  'yellowgreen',
  'lime',
  'yellow',
  'orangeyellow',
  'orange',
  'orangered',
  'brown',
  'grey',
  'gray'
]
export type TuniaoColorValue = 'red' | 'purplered' | 'purple' | 'bluepurple' | 'aquablue' | 'blue' | 'indigo' | 'cyan' | 'teal' | 'green' | 'yellowgreen' | 'lime' | 'yellow' | 'orangeyellow' | 'orange' | 'orangered' | 'brown' | 'grey' | 'gray' | 'white' | 'black'

// 酷炫颜色的数量
const COOL_BG_COLOR_COUNT: number = 16

/**
 * 获取图鸟配色颜色列表
 * @returns 图鸟颜色列表
 */
export const getTuniaoColorList = (): string[] => {
  return colorList
}

/**
 * 随机获取对应类型的颜色类名
 * @param type 获取的颜色类型
 * @returns 对应的颜色类名
 */
export const getRandomColorClass = (type: 'color' | 'bg' | 'cool-bg'): string => {
  if (type === 'cool-bg') {
    return `tn-cool-bg-${getRandom(1, COOL_BG_COLOR_COUNT, true)}`
  } else {
    const index = getRandom(0, colorList.length, true)
    return `tn-${type}-${colorList[index]}`
  }
}

/**
 * 求两个颜色之间的渐变值
 * 
 * @param startColor 开始颜色
 * @param endColor 结束颜色
 * @param step 颜色等分的份额 
 */
export const colorGradient = (startColor: string = 'rgb(0, 0, 0)', endColor: string='rgb(255, 255, 255)', step: number = 10): string[] => {
  let startRGB = hexToRGB(startColor, false) as number[]
  let startR = startRGB[0]
  let startG = startRGB[1]
  let startB = startRGB[2]
  
  let endRGB = hexToRGB(endColor, false) as number[]
  let endR = endRGB[0]
  let endG = endRGB[1]
  let endB = endRGB[2]
  
  // 求差值
  let R = (endR - startR) / step
  let G = (endG - startG) / step
  let B = (endB - startB) / step
  
  let colorArr = []
  for (let i = 0; i < step; i++) {
    // 计算每一步的hex值
    let hex = rgbToHex(`rgb(${Math.round(R * i + startR)}, ${Math.round(G * i + startG)}, ${Math.round(B * i + startB)})`)
    colorArr.push(hex)
  }
  return colorArr
}

/**
 * 将hex的颜色表示方式转换为rgb表示方式
 * 
 * @param color 颜色
 * @param str 是否返回字符串
 * @return rgb的值
 */
export const hexToRGB =(color: string, str: boolean = true): string | number[] => {
  let reg = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$/
  
  color = color.toLowerCase()
  if (color && reg.test(color)) {
    // #000 => #000000
    if (color.length === 4) {
      let colorNew = '#'
      for (let i = 1; i < 4; i++) {
        colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1))
      }
      color = colorNew
    }
    // 处理六位的颜色值
    let colorChange = []
    for (let i = 1; i < 7; i += 2) {
      colorChange.push(parseInt("0x" + color.slice(i, i + 2)))
    }
    if (!str) {
      return colorChange
    } else {
      return `rgb(${colorChange[0]}, ${colorChange[1]}, ${colorChange[2]})`
    }
  } else if (/^(rgb|RGB)/.test(color)) {
    let arr = color.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(',')
    return arr.map(item => Number(item))
  } else {
    return color
  }
}

/**
 * 将rgb的颜色表示方式转换成hex表示方式
 * 
 * @param rgb rgb颜色值
 */
export const rgbToHex = (rgb: string): string => {
  let reg = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$/
  if (/^(rgb|RGB)/.test(rgb)) {
    let color = rgb.replace(/(?:\(|\)|rgb|GRB)*/g, "").split(',')
    let strHex = '#'
    for (let i = 0; i < color.length; i++) {
      let hex = Number(color[i]).toString(16)
      // 保证每个值否是两位数
      hex = String(hex).length === 1 ? 0 + '' + hex: hex
      if (hex === '0') {
        hex += hex
      }
      strHex += hex
    }
    if (strHex.length !== 7) {
      strHex = rgb
    }
    return strHex
  } else if (reg.test(rgb)) {
    let num = rgb.replace(/#/, '').split('')
    if (num.length === 6) {
      return rgb
    } else if (num.length === 3) {
      let numHex = '#'
      for (let i = 0; i < num.length; i++) {
        numHex += (num[i] + num[i])
      }
      return numHex
    }
  }
  return rgb
}

/**
 * 将传入的颜色值转换为rgba字符串
 * 
 * @param color 颜色
 * @param alpha 透明度
 */
export const colorToRGBA = (color: string, alpha: number = 0.3): string => {
  color = rgbToHex(color)
  // 十六进制颜色值的正则表达式
  let reg = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$/
  
  color = color.toLowerCase()
  if (color && reg.test(color)) {
    // #000 => #000000
    if (color.length === 4) {
      let colorNew = '#'
      for (let i = 1; i < 4; i++) {
        colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1))
      }
      color = colorNew
    }
    // 处理六位的颜色值
    let colorChange = []
    for (let i = 1; i < 7; i += 2) {
      colorChange.push(parseInt("0x" + color.slice(i, i + 2)))
    }
    return `rgba(${colorChange[0]}, ${colorChange[1]}, ${colorChange[2]}, ${alpha})`
  } else {
    return color
  }
}