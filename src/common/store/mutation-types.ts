export const ACCESS_TOKEN = 'ACCESS-TOKEN'; // 用户token
export const REFRESH_ACCESS_TOKEN = 'REFRESH-ACCESS-TOKEN'; // 用户刷新token
export const CURRENT_USER = 'CURRENT-USER'; // 当前用户信息
