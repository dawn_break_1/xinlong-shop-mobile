import { defineStore } from "pinia"
import { StoreNames } from '../storeName'
import { SystemInfoType, CustomNavBarType, BoundingButtonType } from '../../types/tuniao-ui'

export const useSystemStore = defineStore(StoreNames.SYSTEM_INFO, {
  state: () => {
    return {
      systemInfo: <SystemInfoType>{},
      customNavbar: <CustomNavBarType>{},
      boundingButton: <BoundingButtonType>{},
    }
  },
  getters: {

  },
  actions: {
    updateCustomNavbarInfo(info: CustomNavBarType) {
      this.customNavbar = info
    },
    updateSystemInfo(info: SystemInfoType) {
      this.systemInfo = info
    },
    updateBoundingButtonInfo(info: BoundingButtonType) {
      this.boundingButton = info
    }
  }
})