
/**
 * 缓存的默认前缀键值
 */
export const storePrefixKeyName: string = 'xinlong'

/**
 * 默认首页地址
 */
export const indexUrl: string = '/pages/index'

/**
 * 自定义顶部导航栏默认高度
 */
export const navbarDefaultHeight: number = 45