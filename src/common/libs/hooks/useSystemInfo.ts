import { useSystemStore } from '../../store/system'
import { storeToRefs } from 'pinia'
import { navbarDefaultHeight } from '../../config'
/**
 * 获取系统信息
 */
export default function useSystemInfo() {

  // 系统缓存状态信息
  const SystemInfoStore = useSystemStore()
  const { systemInfo, customNavbar, boundingButton } = storeToRefs(SystemInfoStore)

  // 更新系统信息
  const updateSystemInfo = () => {
    try {
      const uniSystemInfo = uni.getSystemInfoSync()
      SystemInfoStore.updateSystemInfo({
        screenHeight: uniSystemInfo.screenHeight,
        screenWidth: uniSystemInfo.screenWidth,
        windowHeight: uniSystemInfo.windowHeight,
        windowWidth: uniSystemInfo.windowWidth,
        safeAreaWidth: uniSystemInfo.safeArea?.width || 0,
        safeAreaHeight: uniSystemInfo.safeArea?.height || 0,
        /* #ifndef MP-TOUTIAO */
        statusBarHeight: uniSystemInfo.statusBarHeight,
        /* #endif */
        /* #ifdef MP-ALIPAY */
        titleBarHeight: uniSystemInfo.titleBarHeight
        /* #endif */
      })
    } catch (e) {
      console.error('获取系统信息失败', e)
    }
  }

  // 更新导航栏信息
  const updateCustomNavbarInfo = () => {
    try {
      const uniSystemInfo = uni.getSystemInfoSync()
      const statusBarHeight = uniSystemInfo.statusBarHeight as number
      const titleBarHeight = uniSystemInfo.titleBarHeight as number
      let height: number = 0
      /* #ifndef MP */
      height = statusBarHeight + navbarDefaultHeight
      /* #endif */
      /* #ifdef MP-ALIPAY */
      height = statusBarHeight + titleBarHeight
      /* #endif */
      // #ifdef MP-WEIXIN || MP-BAIDU || MP-TOUTIAO || MP-QQ
      const { bottom, top } = uni.getMenuButtonBoundingClientRect()
      height = bottom + ((top - statusBarHeight < 4) ? 4 : (top - statusBarHeight))
      // #endif
      SystemInfoStore.updateCustomNavbarInfo({
        height: height,
        statusHeight: statusBarHeight
      })
    } catch(e) {
      console.error('更新导航栏信息失败', e)
      // 设置为默认值
      SystemInfoStore.updateCustomNavbarInfo({
        height: navbarDefaultHeight,
        statusHeight: 0
      })
    }
  }

  // 更新小程序胶囊信息
  const updateBoundingButtonInfo = () => {
    try {
      let boundingInfo = {
        width: 0,
        height: 32,
        top: 0,
        bottom: 0,
        right: 0
      }
      // #ifdef MP-WEIXIN || MP-ALIPAY || MP-BAIDU || MP-TOUTIAO || MP-QQ
      const { width, height, bottom, top, right } = uni.getMenuButtonBoundingClientRect()
      boundingInfo.width = width
      boundingInfo.height = height
      boundingInfo.bottom = bottom
      boundingInfo.top = top
      boundingInfo.right = right
      // #endif

      SystemInfoStore.updateBoundingButtonInfo(boundingInfo)
    } catch (e) {
      console.error('更新胶囊按钮容器信息失败', e)
      // 设置默认值
      SystemInfoStore.updateBoundingButtonInfo({
        width: 0,
        height: 32,
        top: 0,
        bottom: 0,
        right: 0
      })
    }
  }

  return {
    systemInfo,
    customNavbar,
    boundingButton,
    updateSystemInfo,
    updateCustomNavbarInfo,
    updateBoundingButtonInfo
  }
}