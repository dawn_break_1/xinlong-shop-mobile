// 订单状态枚举

export enum OrderStatusEnum {
    orders = 0, // 已下单
    pay = 1, //已支付
    pickedup = 2, //已提货
    finish = 3, //已完成
}