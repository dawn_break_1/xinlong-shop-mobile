export const env = 'prod'; // 开发环境dev  生产环境prod

const api_env = {
    dev: {
        //base: 'http://192.168.0.104:8082',

        base: 'https://xinlong.mynatapp.cc'
    },
    prod: {
        //base: 'http://api.fanshishop.com',
        base: 'https://api.xinlongtech.com.cn',
    }
}

const domain_env = {
    dev: {
        h5: 'http://xinlong.natapp1.cc',
        //h5: 'localhost:5173',
    },
    prod: {
        h5: 'http://shop.xinlongtech.com.cn',
    }
}

export const api = env === 'dev' ? api_env.dev : api_env.prod;
export const domain = env === 'dev' ? domain_env.dev : domain_env.prod;


/**
 * 配置文件
 */
// 请求数据域名
// export const BASE_URL: string = 'https://vue3.tuniaokj.com/data/'