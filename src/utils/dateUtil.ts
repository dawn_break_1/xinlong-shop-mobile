import { format } from 'date-fns';

const DATE_TIME_FORMAT = 'yyyy-MM-dd HH:mm';
const DATE_FORMAT = 'yyyy-MM-dd ';

export function formatToDateTime(date: number | Date, formatStr = DATE_TIME_FORMAT): string {
  return format(date, formatStr);
}

export function formatToDate(date: number | Date, formatStr = DATE_FORMAT): string {
  return format(date, formatStr);
}

/**
 * 自定义转换格式
 * @param date 
 * @param formatStr 
 * @returns 
 */
export function formatTo(date: number | Date, formatStr: string) {
  return format(date, formatStr);
}
