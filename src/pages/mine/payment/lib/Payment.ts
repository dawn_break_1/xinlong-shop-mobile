/**
 * 收款数据类型
 */
export type Payment = {
  id: number;
  memberId: number;
  bankCode: string;
  bankName: string;
  accountName: string;
  accountNumber: string;
  alipay: string;
  wxpay: string;
};
