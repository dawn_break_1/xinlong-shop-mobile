/**
 * @description: 订单状态
 */
export enum OrderStatusEnum {
  CREATE = 0,
  PAID_OFF = 1,
  SHIPPED = 2,
  ROG = 3,
  CANCEL = 4,
  COMPLETE = 5,
}

/**
 * 获得订单状态中文
 * @param status 
 * @returns 
 */
export function getOrderStatus(status: number) {
  switch (status) {
    case OrderStatusEnum.CREATE:
      return '待付款';
    case OrderStatusEnum.PAID_OFF:
      return '已付款';
    case OrderStatusEnum.SHIPPED:
      return '已发货';
    case OrderStatusEnum.ROG:
      return '已收货';
    case OrderStatusEnum.CANCEL:
      return '已取消';
    case OrderStatusEnum.COMPLETE:
      return '已完成';
  }
}
