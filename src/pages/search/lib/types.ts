
/**
 * 商品数据类型
 */
export type Goods = {
  id: number;
  goodsClassifyId: number;
  goodsBrandId: number;
  goodsName: string;
  goodsSn: string;
  goodsImgs: string[];
  goodsPrice: number;
  goodsDesc: string;
  classifyName: string;
  brandName: string;
  goodsTags: string[];
  goodsGrade: number;
}