export interface Cart {
  id: number;
  goodsId: number;
  goodsName: string;
  goodsPrice: number; // 商品销售价
  goodsSn: string; //货号
  goodsImgs: string[];
  num: number;
  memberId: number;
  updateTime: number;
  createTime: number;
}
