export  interface Goods {
    id: number;
    goodsName: string;
    goodsClassifyId: number;
    goodsBrandId: number;
    brandName: string;
    goodsSn: string;
    goodsDesc: string;
    goodsPrice: number; // 商品销售价
    goodsCostPrice: number; // 商品成本价
    goodsMarketPrice: number; // 商品市场价格
    goodsSkuSn: string; //货号
    goodsStatus: number;
    goodsImgs: string[]; // 商品图片
    goodsIntro: string;
    goodsGrade: number;
    goodsTags: string[];
    createTime: string;
  }