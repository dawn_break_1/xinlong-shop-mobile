
/**
 * 商品数据类型
 */
export type PurchaseDetail = {
  id: number;
  goodsId: number;
  goodsName: string;
  goodsImgs: string[];
  price: number;
  sellerId: number;
  status: number;
}