export  interface PurchaseGoods {
    id: number;
    goodsId: number;
    goodsName: string;
    goodsDesc: string;
    price: number; // 活动商品销售价
    originalPrice: number; // 活动商品原价
    goodsSkuSn: string; //货号
    status: number;
    goodsImgs: string[]; // 商品图片
    goodsIntro: string;
    startTime: number;
    endTime: number;
    createTime: number;
  }