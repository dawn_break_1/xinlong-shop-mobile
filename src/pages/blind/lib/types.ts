/**
 * 盲盒数据类型
 */
export type BlindBox = {
  id: number;
  blindBoxName: string;
  blindBoxImg: string;
  backImg: string;
  priceRules: PriceRules[],
  produceRules: ProduceRules[],
  showProduceRules: ProduceRules[],
  goodsList: Goods[],
  price: number;
  status: number;
  createTime: number;
};

interface PriceRules {
  id: string;
  name: string;
  price: number;
  buyNum: number;
}

interface ProduceRules {
  id: string;
  img: string;
  name: string;
  odds: number;
}

interface Goods {
  id: number;
  blindBoxId: number;
  goodsId: number;
  goodsName: string;
  goodsImg: string;
  stock: number;
  odds: number;
  tag: string;
  goodsPrice: number;
  createTime: number;
}

export interface WarehouseType {
  blindBoxId: number; //盲盒的id
  blindBoxName: string; // 盲盒的名称
  blindBoxNum: number; // 盲盒抽奖次数
  createTime: number; // 创建时间
  id: number;
  isContentStatus: boolean; 
  itemList: ItemList[]; // 盲盒列表项
  memberId: number; // 会员id
  memberMobile: string; // 会员手机号
  memberName: string; // 会员名字
  nickname: string; // 会员昵称
  orderSn:string; // 订单编号
  payTime: number; // 支付时间
  paymentOrderN: string; // 第三方支付单号
  price: number; // 订单总价
  priceRule: object;
  status: number; // 订单状态
}
interface ItemList {
  blindBoxId: number;
  blindBoxOrderId: number;
  createTime: number; // 创建时间
  goodsId: number;
  goodsImg: string; // 商品图片
  goodsName: string; // 商品名称
  goodsPrice: number; // 商品价格
  id: number;
  memberId:number;
  num: number; // 商品数量
  pickupTime: number; // 提货时间
  shipOrderSn: string; // 发货订单编号
  status:number; // 状态
  isChecked: boolean;
}
