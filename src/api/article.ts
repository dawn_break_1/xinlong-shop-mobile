import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 获取文章内容
 * @param params
 * @returns
 */
export function findArticleById(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `/buyer/article/${params.id}`,
    params,
  });
}
