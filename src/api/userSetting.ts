import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 发送修改密码短信验证码
 * @param params
 * @returns
 */
export function sendSmsCode(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/password/send-code",
    params,
  });
}

/**
 * 修改密码
 * @param params
 * @returns
 */
export function updatePassword(params?: any) {
  return Http.request<RequestType>({
    method: "PUT",
    url: "/buyer/member/password/update",
    params,
    needToken: true,
  });
}
