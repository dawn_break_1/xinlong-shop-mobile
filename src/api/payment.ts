import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 查询收款方式
 * @param params
 * @returns
 */
export function findPayment(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/payment",
    params,
    needToken: true,
  });
}

/**
 * 查询指定会员收款方式
 * @param params
 * @returns
 */
export function findPaymentByMemberId(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `/buyer/member/payment/${params.memberId}`,
    params,
    needToken: true,
  });
}

export function updateBank(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/member/payment/bank",
    data: params,
    isJson: true,
    needToken: true,
  });
}

export function updateAlipay(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/member/payment/alipay",
    params,
    needToken: true,
  });
}

export function updateWxpay(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/member/payment/wxpay",
    params,
    needToken: true,
  });
}
