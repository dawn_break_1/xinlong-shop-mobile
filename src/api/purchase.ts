/**
 * 抢购API
 */
import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 获取今日抢购活动列表
 * @param params
 * @returns
 */
export function getToDay(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "buyer/promotion/rush-purchase/today",
    params,
    needToken: true,
  });
}

/**
 * 获取预告抢购活动列表
 * @param params
 * @returns
 */
export function getNext(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "buyer/promotion/rush-purchase/next",
    params,
    needToken: true,
  });
}

/**
 * 获取特权信息
 * @param params
 * @returns
 */
export function getPrivilegeInfo(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "buyer/promotion/rush-purchase/privilege-info",
    params,
    needToken: true,
  });
}

/**
 * 获取用户是否签名
 * @param params
 * @returns
 */
export function getIsSign(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "buyer/member/sign/is-sign",
    params,
    needToken: true,
  });
}

/**
 * 保存签名
 * @param params
 * @returns
 */
export function saveMemberSign(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "buyer/member/sign",
    data: params,
    needToken: true,
  });
}
