import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

export function findBlindBoxPage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/blind-box/page",
    params,
  });
}

export function findBlindBoxById(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `/buyer/blind-box/${params.id}`,
    params,
  });
}
