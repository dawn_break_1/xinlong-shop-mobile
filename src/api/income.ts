import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 查询
 * @param params
 * @returns
 */
export function findIncomePage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/activity-income/page",
    params,
    needToken: true,
  });
}

/**
 * 查询收益总额
 * @param params
 * @returns
 */
export function findIncomeTotal(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/activity-income/total",
    params,
    needToken: true,
  });
}

/**
 * 查询月 日 预期3个收益
 * @param params
 * @returns
 */
export function findIncomeInfo(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/activity-income/total/info",
    params,
    needToken: true,
  });
}
