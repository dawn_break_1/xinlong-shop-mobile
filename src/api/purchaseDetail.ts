/**
 * 抢购活动明细API
 */
import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 获取抢购活动明细列表
 * @param params
 * @returns
 */
export function findRushPurchaseDetailPage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "buyer/promotion/rush-purchase-detail/page",
    params,
    needToken: true,
  });
}

/**
 * 获取抢购活动明细列表
 * @param params
 * @returns
 */
export function findRushPurchaseDetail(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `buyer/promotion/rush-purchase-detail/${params.id}`,
    params,
    needToken: true,
  });
}

/**
 * 抢购
 * @param params
 * @returns
 */
export function buyRushPurchaseDetail(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: `buyer/promotion/rush-purchase-detail/buy`,
    params,
    needToken: true,
  });
}

/**
 * 获取买家订单
 * @param params
 * @returns
 */
export function findBuyerOrder(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `buyer/promotion/rush-purchase-detail/buyer-order`,
    params,
    needToken: true,
  });
}

/**
 * 上传凭证
 * @param params
 * @returns
 */
export function uploadProof(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: `buyer/promotion/rush-purchase-detail/upload-proof`,
    params,
    needToken: true,
  });
}

/**
 * 获取卖家订单
 * @param params
 * @returns
 */
export function findSellerOrder(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `buyer/promotion/rush-purchase-detail/seller-order`,
    params,
    needToken: true,
  });
}

/**
 * 确认收款
 * @param params
 * @returns
 */
export function confirmReceipt(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: `buyer/promotion/rush-purchase-detail/confirm-receipt`,
    params,
    needToken: true,
  });
}

/**
 * 订单确认提货
 * @param params
 * @returns
 */
export function orderPickUp(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: `buyer/promotion/rush-purchase-detail/pick-up`,
    params,
    needToken: true,
  });
}

/// 以下开始是 另外一个api的内容 都是操作抢购订单 可以放一块
/**
 * 获取订单价格信息
 * @param params
 * @returns
 */
export function getOrderPriceInfo(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `buyer/promotion/put-sale-order/get-order-info`,
    params,
    needToken: true,
  });
}

/**
 * 订单上架前数据准备
 * @param params
 * @returns
 */
export function putSaleOrder(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: `buyer/promotion/put-sale-order`,
    params,
    needToken: true,
  });
}

/**
 * 设置预订会员
 * @param params
 * @returns
 */
export function setBookingMember(params?: any) {
  return Http.request<RequestType>({
    method: "PUT",
    url: `buyer/promotion/rush-purchase-detail/set-booking-member`,
    params,
    needToken: true,
  });
}

/**
 * 删除预订会员
 * @param params
 * @returns
 */
export function delBookingMember(params?: any) {
  return Http.request<RequestType>({
    method: "DELETE",
    url: `buyer/promotion/rush-purchase-detail/del-booking-member`,
    params,
    needToken: true,
  });
}