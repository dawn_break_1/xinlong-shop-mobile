import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 抢购访问统计
 * @param params 
 * @returns 
 */
export function visitRushPurchase(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: `/buyer/page-statistics/rush-purchase/visit`,
    params,
    needToken: true,
  });
}

/**
 * 获取广告列表
 * @param params
 * @returns
 */
export function getAdvList(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/adv/list",
    params,
  });
}
