/**
 * 抢购活动明细API
 */
import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 获取抢购购物车列表
 * @param params
 * @returns
 */
export function findRushPurchaseCartList(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/promotion/rush-purchase-cart/list",
    params,
    needToken: true,
  });
}

/**
 * 商品是否已经加入购物车
 * @param params
 * @returns
 */
export function isHaveRushPurchaseCart(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/promotion/rush-purchase-cart/have",
    params,
    needToken: true,
  });
}

/**
 * 加入购物车
 * @param params
 * @returns
 */
export function addRushPurchaseCart(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/promotion/rush-purchase-cart",
    data: params,
    isJson: true,
    needToken: true,
  });
}

/**
 * 批量购买
 * @param params
 * @returns
 */
export function batchBuy(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/promotion/rush-purchase-cart/buy",
    params,
    needToken: true,
  });
}

/**
 * 单个删除
 * @param params
 * @returns
 */
export function deleteRushPurchaseCart(params?: any) {
  return Http.request<RequestType>({
    method: "DELETE",
    url: `/buyer/promotion/rush-purchase-cart/${params.id}`,
    params,
    needToken: true,
  });
}

/**
 * 清空购物车
 * @param params
 * @returns
 */
export function clearRushPurchaseCart(params?: any) {
  return Http.request<RequestType>({
    method: "DELETE",
    url: "/buyer/promotion/rush-purchase-cart/clear",
    params,
    needToken: true,
  });
}