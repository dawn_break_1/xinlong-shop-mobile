import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 用户登录
 * @param params
 * @returns
 */
export function login(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/login",
    params,
  });
}

/**
 * 获取会员信息
 * @param params
 * @returns
 */
export function getMemberInfo(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/info",
    params,
    needToken: true,
  });
}

/**
 * 刷新token
 * @param params
 * @returns
 */
export function refreshToken(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/token",
    params,
  });
}

/**
 * 发送短信验证码
 * @param params
 * @returns
 */
export function sendSmsCode(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/send-code",
    params,
  });
}

/**
 * 注册
 * @param params
 * @returns
 */
export function register(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/member/register",
    params,
  });
}

/**
 * 获取openid
 * @param params
 * @returns
 */
export function getOpenId(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/wx/pay/openid",
    params,
    needToken: true,
  });
}

/**
 * 获取用户余额
 * @param params
 * @returns
 */
export function getBalance(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/balance",
    params,
    needToken: true,
  });
}

/**
 * 获取用户余额信息-余额、累计收益、累计提现
 * @param params
 * @returns
 */
export function getBalanceInfo(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/balance-info",
    params,
    needToken: true,
  });
}


/**
 * 根据手机号获取用户昵称
 * @param params
 * @returns
 */
export function getNickname(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/nickname",
    params,
    needToken: true,
  });
}


/**
 * 微信小程序登录，会自动注册
 * @param params
 * @returns
 */
export function loginByWxMini(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/wx/mini/login",
    params
  });
}

/**
 * 测试权限用 , 可删除
 * @param params
 * @returns
 */
export function test(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/test",
    params,
    needToken: true,
  });
}

