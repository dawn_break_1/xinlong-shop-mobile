import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 获取系统配置
 * @param params
 * @returns
 */
export function getSysSetting(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/sys/setting",
    params,
    needToken: true,
  });
}