import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 加入购物车
 * @param params
 * @returns
 */
export function addCard(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/order/cart",
    data: params,
    isJson: true,
    needToken: true,
  });
}

/**
 * 查询
 * @param params
 * @returns
 */
export function findCart(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/order/cart",
    params,
    needToken: true,
  });
}

/**
 * 修改购物车数量
 * @param params
 * @returns
 */
export function updateNum(params?: any) {
  return Http.request<RequestType>({
    method: "PUT",
    url: "/buyer/order/cart/num",
    params,
    needToken: true,
  });
}

/**
 * 创建订单
 * @param params
 * @returns
 */
export function createOrder(params?: any, data?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/order/order/create-order",
    params: params,
    data: data,
    isJson: true,
    needToken: true,
  });
}

/**
 * 查询订单
 * @param params
 * @returns
 */
export function findOrder(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/order/order",
    params,
    needToken: true,
  });
}

/**
 * 得到物流信息
 * @param params
 * @returns
 */
export function getLogistics(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/order/order/query-logistics",
    params,
    needToken: true,
  });
}
/**
 * 得到物流信息
 * @param params
 * @returns
 */
export function confirmReceipt(params?: any) {
  return Http.request<RequestType>({
    method: "PUT",
    url: `/buyer/order/order/confirm-receipt/${params.id}`,
    params,
    needToken: true,
  });
}

/**
 * 订单支付
 * @param params
 * @returns
 */
export function payOrder(params?: any, data?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/order/order/order-pay",
    params: params,
    data: data,
    isJson: true,
    needToken: true,
  });
}


