import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 以图生图API
 * @param params 
 * @returns 
 */
export function imageToImage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/img/img-to-img",
    params,
    needToken: true,
  });
}

