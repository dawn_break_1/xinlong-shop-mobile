import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 新增用户提现申请
 * @param params
 * @returns
 */
export function saveWithdrawal(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/member/withdrawal",
    data: params,
    isJson: true,
    needToken: true,
  });
}

/**
 * 获取提现记录列表
 * @param params
 * @returns
 */
export function findWithdrawalPage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/withdrawal/page",
    params,
    needToken: true,
  });
}
