import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 查询
 * @param params
 * @returns
 */
export function findAddress(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/address/list",
    params,
    needToken: true,
  });
}

export function updateAddress(params?: any) {
  return Http.request<RequestType>({
    method: "PUT",
    url: `/buyer/member/address/${params.id}`,
    data: params,
    isJson: true,
    needToken: true,
  });
}

export function saveAddress(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/member/address",
    data: params,
    isJson: true,
    needToken: true,
  });
}

export function deleteAddress(params?: any) {
  return Http.request<RequestType>({
    method: "DELETE",
    url: `/buyer/member/address/${params.id}`,
    data: params,
    needToken: true,
  });
}

/**
 * 查询地址数量
 * @param params
 * @returns
 */
export function findAddressNum(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/address/num",
    params,
    needToken: true,
  });
}

/**
 * 得到默认地址
 * @param params
 * @returns
 */
export function getDefAddress(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/address/def-address",
    params,
    needToken: true,
  });
}
