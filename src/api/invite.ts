import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 获取受邀人列表
 * @param params
 * @returns
 */
export function findInviteePage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/invitee-list",
    params,
    needToken: true,
  });
}

/**
 * 获取余额明细列表
 * @param params
 * @returns
 */
export function findBalancePage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/member/balance/page",
    params,
    needToken: true,
  });
}
