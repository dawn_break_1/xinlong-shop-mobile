import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 通过首页位置获取盲盒商品列表
 * @param params
 * @returns
 */
export function getHomeSiteBlindBox(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/home-site",
    params,
  });
}

/**
 * 获取首页展示的商品 按照发布时间排序
 * @param params
 * @returns
 */
export function getHomeSiteGoods(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/home-site/goods-list",
    params,
  });
}
