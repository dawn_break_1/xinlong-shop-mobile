import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

export function createBlindBoxOrder(params?: any) {
  return Http.request<RequestType>({
    method: "POST",
    url: "/buyer/blind-box-order/create-order",
    params,
    needToken: true,
  });
}

/**
 * 查询盲盒订单内商品
 * @param params
 * @returns
 */
export function getBlindOrderItem(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `/buyer/blind-box/blind-box-order/item/list/${params.orderId}`,
    params,
    needToken: true,
  });
}