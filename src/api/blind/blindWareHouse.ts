import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

// 获取当前用户的仓库数据
export const getWareHouse = (params?: any) => {
  return Http.request<RequestType>({
    url: "/buyer/blind-box-order/warehouse",
    method: "GET",
    needToken: true,
    params
  })
}

/**
 * 创建商城订单（盲盒发货）
 * @param params 
 * @param data 
 * @returns 
 */
export function createShopOrder(params?: any, data?: any) {
    return Http.request<RequestType>({
        url:'/buyer/blind-box-order/create-shop-order',
        method: "POST",
        params: params,
        data: data,
        isJson: true,
        needToken: true,
    })
}