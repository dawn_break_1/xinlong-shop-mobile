import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

/**
 * 得到微信获取code的url
 * @param params
 * @returns
 */
export function getCodeUrl(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/wx/pay/get-code-url",
    params,
  });
}