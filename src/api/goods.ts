import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

export function findGoodsPage(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/goods/page",
    params,
  });
}

export function findGoodsById(params?: any) {
  return Http.request<RequestType>({
    method: "GET",
    url: `/buyer/goods/${params.goodsId}`,
    params,
  });
}
