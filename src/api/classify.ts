import type { RequestType } from "@/utils/request";
import Http from "@/utils/request";

export function getClassify() {
  return Http.request<RequestType>({
    method: "GET",
    url: "/buyer/classify/children"
  });
}
