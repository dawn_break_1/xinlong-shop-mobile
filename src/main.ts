import { createSSRApp } from "vue"
import App from "./App.vue"
// 存储状态管理
import store from './common/store'

export function createApp() {
  const app = createSSRApp(App)
  app.use(store)
  return {
    app
  }
}